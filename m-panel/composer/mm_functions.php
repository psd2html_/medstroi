<?php
if(!function_exists('getParentByID')){
	function getParentByID($id){
		global $modx;
		$id = (int)$id;
		$q = "SELECT parent FROM ".$modx->getFullTableName('site_content')." WHERE id='".$id."'";
		return $modx->db->getValue($q);
	}
}
if(!function_exists('getHideTpl') && function_exists('mm_hideTemplates')){
	function getHideTpl($category){
		global $modx;
		$tpl = array();
		$category = (int)$category;
		$q = $modx->db->query("SELECT id FROM ".$modx->getFullTableName('site_templates')." WHERE `category`!=".$category);
		$q = $modx->db->makeArray($q);
		foreach($q as $item){
			$tpl[] = $item['id'];
		}
		$tpl = implode(",", $tpl);
		if(!empty($tpl)){
			mm_hideTemplates($tpl);
		}
	}
}