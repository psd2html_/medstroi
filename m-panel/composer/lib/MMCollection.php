<?php

class MMCollection{
	protected static $hideFields = array();
	protected static $moveFields = array();
	protected static $tabs = array();
	public static $debug = false;

	public static function hideField($name){
		if(is_scalar($name)){
			self::$hideFields[] = $name;
			self::unsetMoveField($name);
		} else if(is_array($name)){
			foreach($name as $field){
				self::hideField($field);
			}
		}
	}

	public static function moveField($name, $tab){
		if(is_scalar($tab)){
			if(is_scalar($name)){
				static::unsetMoveField($name);
				self::$moveFields[$tab][] = $name;
			}else if(is_array($name)){
				foreach($name as $field){
					self::moveField($field, $tab);
				}
			}
		}
	}
	public static function showField($field){
		if(is_scalar($field)){
			foreach(self::$hideFields as $id => $key){
				if($key == $field){
					unset(self::$hideFields[$id]);
					break;
				}
			}
		}else if(is_array($field)){
			foreach($field as $key){
				static::showField($key);
			}
		}
	}
	public static function unsetMoveField($name){
		foreach(self::$moveFields as &$tab){
			foreach($tab as &$field){
				if($name == $field){
					$field = null;
					break;
				}
			}
		}
	}

	public static function makeTab($id, $name = null, $width = ''){
		if(empty($name)){
			if(isset(static::$tabs[$id]['name'])){
				$name = static::$tabs[$id]['name'];
			}else{
				$name = $id;
			}
		}
		if(empty($width) && isset(static::$tabs[$id]['width'])) {
			$width = $tabs[$id]['width'];
		}
		static::$tabs[$id] = array('name' => $name, 'width' => $width);
	}
	public static function unsetTab($id){
		if(is_scalar($id)){
			unset(static::$tabs[$id]);
		} else if(is_array($id)){
			foreach($id as $tab){
				static::unsetTab($tab);
			}
		}
	}

	public static function callMakeTabs(){
		foreach(static::$tabs as $key => $params){
			if(!empty(static::$moveFields[$key])){
				mm_createTab($params['name'], $key, '', '', '', $params['width']);
			}
		}
	}
	public static function callHideFields(){
		$fields = array_unique(self::$hideFields);
		if(static::$debug){
			echo "<h1>hideFields</h1>";
			echo "<pre>";
			print_r($fields);
			echo "</pre>";
		}
		if(!empty($fields)){
			mm_hideFields(implode(",", $fields));
		}
	}

	public static function callMoveFields(){
		foreach(static::$moveFields as $tab => $fields){
			if(isset(static::$tabs[$tab])){
				$fields = array_unique($fields);
				if(!empty($fields)){
					if(static::$debug){
						echo "<h1>moveFields to {$tab}</h1>";
						echo "<pre>";
						print_r($fields);
						echo "</pre>";
					}
					mm_moveFieldsToTab(implode(",", $fields), $tab, "", "");
				}
			}
		}
	}

}