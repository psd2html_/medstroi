<?php namespace MedStroy\Sender;

class Contact extends \AN\Sender{
	public $required = array(
		'fio' => 'Ваше имя',
		'email' => 'Ваш e-mail',
		'message' => 'Текст сообщения'
	);

	public function prepare(array $data = array()){
		$useData = array(
			'fio' => e(full_one_space(mb_ucfirst(get_key($data, 'fio', '', 'is_scalar')))),
			'email' => get_key($data, 'email', ''),
			'phone' => get_key($data, 'phone', ''),
			'message' => e(one_space(one_new_line(get_key($data, 'message', '', 'is_scalar'))))
		);
		return $useData;
	}

	public function checkRequiredFields($data, $required = array()){
        $out = null;
        if(!is_array($data)){
	    	$data = array();
	    }
	    $out = array();
	    foreach($required as $key => $name){
	    	$tmp = get_key($data, $key, '', function($val){
	    		return is_scalar($val);
	    	});
	    	switch(true){
	    		case (full_one_space($tmp) === ''):{
	    			$out[] = $name;
	    			break;
	    		}
	    		case ($key == 'email' && check_email($tmp, false)):{
	    			$out[] = $name;
	    			break;
	    		}
	    		case ($key == 'phone' && !\is_phone($tmp)):{
	    			$out[] = $name;
	    			break;
	    		}
	    	}
	    }
    	return $out;
	}
}