<?php namespace MedStroy\Sender;

use MedStroy\MODxAPI\modSubscribe;

class Subscribe extends \AN\Sender{
	public $required = array(
		'email' => 'Ваш e-mail'
	);

	public function prepare(array $data = array()){
		$useData = array(
			'email' => get_key($data, 'email', ''),
		);
		return $useData;
	}

	public function checkRequiredFields($data, $required = array()){
        $out = null;
        if(!is_array($data)){
	    	$data = array();
	    }
	    $out = array();
	    foreach($required as $key => $name){
	    	$tmp = get_key($data, $key, '', function($val){
	    		return is_scalar($val);
	    	});
	    	switch(true){
	    		case (full_one_space($tmp) === ''):{
	    			$out[] = $name;
	    			break;
	    		}
	    		case ($key == 'email' && check_email($tmp, false)):{
	    			$out[] = $name;
	    			break;
	    		}
	    	}
	    }

	    if(empty($out)){
		    $modSubscribe = new modSubscribe($this->modx);
		    $modSubscribe->useIgnore(true);
		    $email = get_key($data, 'email', '', 'is_scalr');
		    $id = null;
		    if($modSubscribe->isUniq($email)){
			    $id = $modSubscribe->create(array(
			    	'email' => $email,
			    	'ip' => get_user_ip(),
			    	'user_agent' => get_key($_SERVER, 'HTTP_USER_AGENT', '', 'is_scalar'),
			    	'referer' => get_key($_SERVER, 'HTTP_REFERER', '', 'is_scalar')
			    ))->save();
			}
			if(empty($id)){
			    $out[] = 'Данный e-mail уже подписан на рассылку';
			}
		}
    	return $out;
	}
}