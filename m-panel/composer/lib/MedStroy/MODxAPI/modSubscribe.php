<?php namespace MedStroy\MODxAPI;
use autoTable;

class modSubscribe extends autoTable{
	protected $table = 'subscribers';

	public function isUniq($email)
    {
        $old = $this->get('email');
        $flag = $this->set('email', $email)->checkUnique($this->table, 'email');
        $this->set('email', $old);
        return $flag;
    }

    public function create($data = array())
	{
		parent::create($data);
		$this->set('created_at', date('Y-m-d H:i:s', time() + $this->modx->config['server_offset_time']));
		return $this;
	}

	public function save($fire_events = null, $clearCache = false) {
		if (empty($this->field['email'])){
			$this->log['emptyEmail'] = 'Email is empty in <pre>' . print_r($this->field, true) . '</pre>';
			return false;
		}
		if($out = parent::save($fire_events, $clearCache)) {
			//with(new \logHandler)->initAndWriteLog("Сохранен заказ ".$this->getID());
			//$this->invokeEvent('OnAfterSaveOrder', array('obj' => $this), $fire_events);
		}
		return $out;
	}

	public function set($key, $value)
	{
		switch($key) {
			case 'ip':
			case 'referer':
			case 'user_agent':
			case 'email':
				$value = is_scalar($value) ? one_space(one_new_line($value)) : '';
				break;
			case 'created_at':
				if(!is_scalar($value)){
					$value = '';
				}
				break;
		}
		parent::set($key, $value);
		return $this;
	}
}