<?php namespace AN;

include_once(MODX_BASE_PATH."assets/snippets/DocLister/lib/DLTemplate.class.php");
include_once(MODX_BASE_PATH."assets/lib/APIHelpers.class.php");

class Sender{
	public $required = array();

	public $toMail = 'modx@agel-nash.ru';
    public $configField = null;
	public $messageTpl = '';
	public $filesField = null;
	protected $subject = '';

	protected $modx;
	protected $tplObj = null;
	protected $alert = array();
    protected $files = array();
	protected $params = array();

	public function __construct($modx, $subject = null, $toMail = null, $messageTpl = null, $configField = null){
        $this->modx = $modx;

        $this->setSubject($subject);
        if(!empty($toMail)){
            $this->toMail = $toMail;
        }
        $this->messageTpl = $messageTpl;
        $this->configField = $configField;
        $this->tplObj = \DLTemplate::getInstance($this->modx);
		$this->params = $this->modx->event->params;
    }
    public function getFullConfigField(){
        return empty($this->configField) ? '' : 'sender_'.$this->configField;
    }
	public function getNumMail(){
        $num = (!empty($this->configField)) ? (int)$this->modx->getConfig($this->getFullConfigField()) : 0;
        return ++$num;
    }
    public function updateEmailNumber(){
        if(!empty($this->configField)){
            $table = $this->modx->getFullTableName('system_settings');
            $query = "SELECT count(*) FROM ".$table." WHERE `setting_name` = '".$this->getFullConfigField()."'";
            if($this->modx->db->getValue($query)){
                $this->modx->db->update(
                    array(
                        'setting_value' => $this->getNumMail()
                    ), $table, "`setting_name` = '".$this->getFullConfigField()."'"
                );
            }else{
                $this->modx->db->insert(
                    array(
                        'setting_value' => $this->getNumMail(),
                        'setting_name' => $this->getFullConfigField()
                    ), $table
                );
            }
            $this->modx->config = array();
            unlink(MODX_BASE_PATH.'assets/cache/siteCache.idx.php');
            $this->modx->getSettings();
        }
        return $this;
    }

    public function setSubject($subject){
    	$this->subject = is_scalar($subject) ? $subject : '@CODE: Сообщение с сайта '.$this->modx->getConfig('site_name');
    	return $this;
    }
    public function getSubject(array $data = array()){
        if(!empty($this->configField)){
            $num = ' №'.$this->getNumMail();
        }else{
            $num = '';
        }
        return $this->tplObj->parseChunk($this->subject, $data) . $num;
    }
	public function send($data){
        $data = $this->prepare($data);
        $files = (!empty($this->filesField) && !empty($data[$this->filesField]) && is_array($data[$this->filesField])) ? $data[$this->filesField] : array();
        $this->files = array_merge($this->files, $files);

        $flag = $this->checkRequiredFields($data, $this->required);
        if(empty($flag) && is_array($flag)){
            $data = $this->mixedData($data);

            $flag = $this->mail(
                array(
                    'to' => $this->toMail,
                    'subject' => $this->getSubject($data),
                    'fromname' => $this->modx->getConfig('site_name')
                ),
                $this->tplObj->parseChunk($this->messageTpl, $data, true),
                $this->files
            );
            if($flag){
                $this->eventAfterSend($data, $this->files);
                $this->updateEmailNumber();
            }
            foreach($this->files as $f){
                unlink(MODX_BASE_PATH.$f);
            }
        }
        return $flag;
	}
    public function mixedData($data){
        $main = get_key($_POST, 'main', array(), 'is_array');
        return array_merge($data, array(
            'userIP' => \APIhelpers::getUserIP(),
            'docID' => \APIhelpers::getkey($main, 'modxDocid', $this->modx->getConfig('error_page')),
            'REAL_REFERER' => \APIhelpers::e(\APIhelpers::getkey($main, 'modxReferer', '')),
            'HTTP_USER_AGENT' => \APIhelpers::e(\APIhelpers::getkey($_SERVER, 'HTTP_USER_AGENT', '')),
            'HTTP_REFERER' => \APIhelpers::e(\APIhelpers::getkey($_SERVER, 'HTTP_REFERER', ''))
        ));
    }
    public function checkRequiredFields($data, $required = array()){
        if(!is_array($data)){
            $data = array();
        }
        $out = array();
        foreach($required as $key => $name){
            $tmp = \APIhelpers::getkey($data, $key, '');
            if((is_scalar($tmp) && $tmp == '') || (is_array($tmp) && $tmp == array())){
                $out[] = $name;
            }
        }
        return $out;
    }

    public function mail($params=array(), $msg='', $files = array())
    {
        $modx = $this->modx;
        if(isset($params) && is_string($params))
        {
            if(strpos($params,'=')===false)
            {
                if(strpos($params,'@')!==false) $p['to']      = $params;
                else                            $p['subject'] = $params;
            }
            else
            {
                $params_array = explode(',',$params);
                foreach($params_array as $k=>$v)
                {
                    $k = trim($k);
                    $v = trim($v);
                    $p[$k] = $v;
                }
            }
        }
        else
        {
            $p = $params;
            unset($params);
        }
        if(isset($p['sendto'])) $p['to'] = $p['sendto'];

        if(isset($p['to']) && preg_match('@^[0-9]+$@',$p['to']))
        {
            $userinfo = $modx->getUserInfo($p['to']);
            $p['to'] = $userinfo['email'];
        }
        if(isset($p['from']) && preg_match('@^[0-9]+$@',$p['from']))
        {
            $userinfo = $modx->getUserInfo($p['from']);
            $p['from']     = $userinfo['email'];
            $p['fromname'] = $userinfo['username'];
        }
        if($msg==='' && !isset($p['body']))
        {
            $p['body'] = $_SERVER['REQUEST_URI'] . "\n" . $_SERVER['HTTP_USER_AGENT'] . "\n" . $_SERVER['HTTP_REFERER'];
        }
        elseif(is_string($msg) && 0<strlen($msg)) $p['body'] = $msg;

        $modx->loadExtension('MODxMailer');
        $sendto = (!isset($p['to']))   ? $modx->config['emailsender']  : $p['to'];
        $sendto = explode(',',$sendto);
        foreach($sendto as $address)
        {
            list($name, $address) = $modx->mail->address_split($address);
            $modx->mail->AddAddress($address,$name);
        }
        if(isset($p['cc']))
        {
            $p['cc'] = explode(',',$p['cc']);
            foreach($p['cc'] as $address)
            {
                list($name, $address) = $modx->mail->address_split($address);
                $modx->mail->AddCC($address,$name);
            }
        }
        if(isset($p['bcc']))
        {
            $p['bcc'] = explode(',',$p['bcc']);
            foreach($p['bcc'] as $address)
            {
                list($name, $address) = $modx->mail->address_split($address);
                $modx->mail->AddBCC($address,$name);
            }
        }
        if(isset($p['from']) && strpos($p['from'],'<')!==false && substr($p['from'],-1)==='>')
            list($p['fromname'],$p['from']) = $modx->mail->address_split($p['from']);
        $modx->mail->From     = (!isset($p['from']))  ? $modx->config['emailsender']  : $p['from'];
        $modx->mail->FromName = (!isset($p['fromname'])) ? $modx->config['site_name'] : $p['fromname'];
        $modx->mail->Subject  = (!isset($p['subject']))  ? $modx->config['emailsubject'] : $p['subject'];
        $modx->mail->Body     = $p['body'];
        if (isset($p['type']) && $p['type'] == 'text') $modx->mail->IsHTML(false);
        foreach($files as $f){
            if(file_exists(MODX_BASE_PATH.$f) && is_file(MODX_BASE_PATH.$f) && is_readable(MODX_BASE_PATH.$f)){
                $modx->mail->AddAttachment(MODX_BASE_PATH.$f);
            }
        }
        $rs = $modx->mail->send();
        return $rs;
    }

	public function setAlert($mode, $tpl){
		return $this->setAlertMode($mode)->setAlertTpl($tpl);
	}
	public function setAlertTpl($tpl){
		$this->alert['tpl'] = is_scalar($tpl) ? $tpl : '';
		return $this;
	}
	public function setAlertMode($mode){
		$this->alert['mode'] = is_scalar($mode) ? $mode : '';
		return $this;
	}

	public function getAlert(){
		return $this->alert;
	}
	public function getAlertTpl(){
		return \APIhelpers::getkey($this->alert, 'tpl', '');
	}
	public function getAlertMode(){
		return \APIhelpers::getkey($this->alert, 'mode', 'chunk');
	}

	public function renderAlert(){
        switch($this->getAlertMode()){
            case 'redirect':{
                $this->modx->jsonStatus = 'ok';
                $this->modx->jsonMixed['url'] = $this->modx->makeUrl($this->getAlertTpl(), '', '', 'full');
                $out = '';
                break;
            }
			case 'page':{
                $this->modx->jsonStatus = 'ok';
				$out = $this->modx->sendForward($this->getAlertTpl());
				break;
			}
			case 'chunk':
			default:{
				$out = $this->tplObj->getChunk($this->getAlertTpl());
				break;
			}
		}
		return $out;
	}

	public function prepare(array $data = array()){
		return $data;
	}

	public function eventAfterSend($data, $files){
        return true;
	}
}