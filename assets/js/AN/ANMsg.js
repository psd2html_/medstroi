;(function($){
    $(function() {
        $.ANMsg = {
            info: function(text, mode){
                mode = mode || 'modal';
				switch(mode){
                    case 'alert':{
                        if (typeof $.fn.jGrowl !== 'undefined') {
                            $.jGrowl(text, {theme: 'ommsg-info'});
                        } else {
                            alert(text);
                        }
                        break;
                    }
                    default:{
                        $.sender.fancy(text);
                    }
                }
            },
            notice: function(text, mode){
                mode = mode || 'modal';
                switch(mode){
                    case 'alert':{
                        if (typeof $.fn.jGrowl !== 'undefined') {
                            $.jGrowl(text, {theme: 'ommsg-success'});
                        } else {
                            alert(text);
                        }
                        break;
                    }
                    default:{
                        $.sender.fancy('<div class="errorMessage"><h1>Произошла ошибка</h1><br />' + text + '</div>');
                    }
                }
            },
            error: function(text, mode){
                mode = mode || 'modal';
                switch(mode){
                    case 'alert':{
                        if (typeof $.fn.jGrowl !== 'undefined') {
                            $.jGrowl(text, {theme: 'ommsg-error'});
                        } else {
                            alert(text);
                        }
                        break;
                    }
                    default:{
                        $.sender.fancy('<div class="errorMessage"><h1>Произошла ошибка</h1><br />' + text + '</div>');
                    }
                }
            },
            fail: function(text, mode){
                mode = mode || 'modal';
                switch(mode){
                    case 'alert':{
                        if (typeof $.fn.jGrowl !== 'undefined') {
                            $.jGrowl(text, {theme: 'ommsg-error'});
                        } else {
                            alert(text);
                        }
                        break;
                    }
                    default:{
                        $.sender.fancy('<div class="errorMessage"><h1>Произошла ошибка</h1><br />' + text + '</div>', true);
                    }
                }
            }
        };
    });
})(jQuery);