;(function($){
    $(function() {
        /**
         * Хелпер функции для jQuery
         */
         $.jHelper = {
            htmlDecode: function(str){
                return $('<div/>').html(str).text();
            },
            htmlEncode: function(str){
                return $("<div/>").text(str).html();
            },
            getObjFromString: function (str, selector){
                return $(selector, $('<output>').append($.parseHTML(str)));
            },
            getHtmlFromObj: function(obj){
                return $('<div/>').append(obj.clone()).html();
            },
            issetData: function(obj, key){
                return (typeof $(obj).data(key) !== 'undefined')
            },
            defaultData: function(obj, key, value){
                var ret;
                if($.jHelper.issetData($(obj), key)){
                    ret = $(obj).data(key);
                }else{
                    $(obj).data(key, value);
                    ret = value;
                }
                return ret;
            },
            tagName: function(el){
                return $(el).prop("tagName");
            },
            lowerCase: function(val) {
                return val.toLowerCase();
            },
            /*
            * Небольшой шаблонизатор на JavaScript
            * @see: http://javascript.ru/unsorted/templating
            */
            renderTPL: function(template, data) {
                var out = '';
                if (template != '') {
                    out = template.replace(/[\r\t\n]/g, " ")
                        .split("<%").join("\t")
                        .replace(/((^|%>)[^\t]*)'/g, "$1\r")
                        .replace(/\t=(.*?)%>/g, "',$1,'")
                        .split("\t").join("');")
                        .split("%>").join("p.push('")
                        .split("\r").join("\\'");
                    out = new Function("obj", "var p=[],print=function(){p.push.apply(p,arguments);};with(obj){p.push('" + out + "');}return p.join('');")(data);
                }
                return out;
            },

            formatSize: function(length, type){
                var i = 0;
                type = type || ['б','Кб','Мб','Гб','Тб','Пб'];
                while((length / 1000 | 0) && i < type.length - 1) {
                    length /= 1024;
                    i++;
                }
                return length.toFixed(2) + ' ' + type[i];
            }
        };
    });
})(jQuery);