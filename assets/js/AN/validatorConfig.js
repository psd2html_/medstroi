;(function($){
    $(function() {
        $.validatorConfig = {
            errorPlacement: function(error, element) {
                error.insertAfter(element);
            },
            submitHandler: function(form){
                $.sender.ajax(form);
            }
        };
    });
})(jQuery);