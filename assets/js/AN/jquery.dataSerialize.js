;(function($){
	/** @see https://www.bram.us/2008/10/27/jqueryserializeanything-serialize-anything-and-not-just-forms/ */
	$.fn.dataSerialize = function(prefix) {
		prefix = prefix || '';
		var toReturn    = [];
		var els         = $(this).data();
		$.each(els, function(key, val){
			var useKey = (prefix === '' ? key : (prefix + '[' + key + ']'));
			toReturn.push( encodeURIComponent(useKey) + "=" + encodeURIComponent( val ) );
		});
		return toReturn.join("&").replace(/%20/g, "+");
	};
})(jQuery);