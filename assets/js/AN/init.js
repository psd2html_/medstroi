window.FileAPI = {
    debug: false,
    staticPath: '/js/jquery.fileapi/FileAPI/'
};
;(function($){
    $(function() {
        if (typeof $.fn.jGrowl !== 'undefined') {
            $.jGrowl.defaults.closerTemplate = '<div>[ ������� ]</div>';
            $.jGrowl.defaults.life = 10000;
        }

        $.OMMsg = {
            info: function(text){
                $.jGrowl(text, {theme: 'ommsg-info'});
            },
            notice: function(text){
                 $.jGrowl(text, {theme: 'ommsg-success'});
            },
            error: function(text){
                $.jGrowl(text, {theme: 'ommsg-error'});
            }
        };
        if(typeof FileAPI != 'undefined'){
            var TPLFile = '';
            if($('#fileInput').length) {
                TPLFile = $('#fileInput').html();
            }
            if($("#uploader").length){
                var form = $("#uploader").parents('form');
                $(".sendForm", form).hide();
                $('#uploader').fileapi({
                    url: '/upload.php?&pageID=' + $.jHelper.defaultData('body', 'modx-docid', 0),
                    autoUpload: true,
                    multiple: false,
                    clearOnSelect: true,
                    elements: {
                        size: '.js-size',
                        active: { show: '.js-upload', hide: '.js-browse' },
                        progress: '.js-progress'
                    },
                    onBeforeUpload: function(){
                    },
                    onComplete: function(evt, ui){
                        if(!ui.error){
                            if( ui.result){
                                $("#fileList").append($.jHelper.renderTPL(TPLFile, {
                                    'path': ui.result,
                                    'size': $.jHelper.formatSize(ui.file.size),
                                    'name': ui.file.name,
                                }));
                                $.OMMsg.notice("��� ���� ������� �������� �� ������");
                            }else{
                                $.OMMsg.error('�� ������� ��������� ���� �� ������');
                            }
                        }else{
                            $.OMMsg.error('�� ����� �������� ����� ��������� ������');
                        }

                    },
                });
            }
        }

        function ajaxLink(){
            $('body').on('click', 'a:data(modx=ajax)', function(e){
                e.preventDefault();
                $.ajax({
                    url: $(this).attr('href'),
                    data: {
                        data: $(this).data(),
                        main: $("body").data()
                    },
                    type: "GET"
                }).done(function(responce){
                    $.sender.fancy(responce);
                   initAjax();
                }).fail(function(){
                    $.ANMsg.error($.sender.lang.errorLoad);
                });
            });
        }
        ajaxLink();

        /** AJAX �������� ����� */
        function ajaxFormSend(){
            var validateForm = $('form:not(.no-validate)').not(':data(modx=ajax)');
            if(validateForm.length){
                var noAjaxConfig = $.extend({}, $.validatorConfig, {
                    submitHandler: function(form){
                        form.submit();
                    }
                });
                validateForm.each(function(index) {
                    $(this).validate(noAjaxConfig);
                });
            }

            var ajaxForm = $('form:data(modx=ajax)');
            if(ajaxForm.length){
                ajaxForm.each(function(index) {
                    $(this).validate($.validatorConfig);
                });
            }
        }

        $('body').on('click', '.modal-link', function(e){
            e.preventDefault();
            var selector = $.jHelper.defaultData($(this), 'inline', '');
            if(selector !== '' && $(selector).length){
                $.sender.fancy($(selector).html());
                ajaxFormSend();
            }
        });

        function initAjax(){
            if($('select').length){
                $('select').ikSelect({
                    autoWidth: false,
                    ddFullWidth: false
                });
            }
            $("input[name=phone]").mask("+7 (999) 999-99-99");
            ajaxFormSend();
        }
        initAjax();

        $('a.fancy').fancybox();

        $('body').on('click', '.noclick', function(e){
            e.preventDefault();
        });
    });
})(jQuery);