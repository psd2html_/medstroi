<?php
/*
** TOP NAV MENU SNIPPET
*/

$params = array(
	'idType' => 'documents',
	'documents' => $modx->config['cfg_top_menu_items'],
	'sortType'=>'doclist',
	'tplProducts' => '@CODE: <li class="has-child [+dl.class+]"><a href="[+url+]" title="[+e.title+]">[+title+]</a><div class="mainmenu-dd"><div class="mainmenu-dd__content"><div class="wrap clearfix">[+cats+][+brand+]</div></div></div></li>',
	'tpl' => '@CODE: <li class="[+dl.class+]"><a href="[+url+]" [+attr+]>[+title+]</a></li>',
	'ownerTPL' => '@CODE: <ul class="mainmenu__list">[+dl.wrap+]</ul>',
	'currentTpl'=>'',
	'prepare' => function($data, $modx, $_DL){
			if($data['id'] == 48){
				$data['attr'] = 'target="_blank"';
			}
			  if($data['id'] == 41){
			  	 	$cats = $modx->getDocumentObject('id',5);
			    	$data['cats'] = $modx->runSnippet('DocLister', array('idType' => 'parents', 'parents' => 5, 'order'=>'ASC',
			    			'ownerTPL'=>'@CODE: <div class="mainmenu-dd__childmenu cat"><div class="mainmenu-dd__childmenu-title">'.$cats['pagetitle'].'</div><ul>[+dl.wrap+]</ul></div>',
			    			'tpl'=>'@CODE: <li><a href="[+url+]" title="[+e.title+]">[+title+]</a></li>'
			    		));
			    	$brends = $modx->getDocumentObject('id',4);
			     	$data['brand'] = $modx->runSnippet('DocLister', array('idType' => 'parents', 'parents' => 4, 'order'=>'ASC',
			    			'ownerTPL'=>'@CODE: <div class="mainmenu-dd__childmenu brend"><div class="mainmenu-dd__childmenu-title">'.$brends['pagetitle'].'</div><ul>[+dl.wrap+]</ul></div>',
			    			'tpl'=>'@CODE: <li><a href="[+url+]" title="[+e.title+]">[+title+]</a></li>'
			     		));
			     	$_DL->renderTPL = $_DL->getCFGDef('tplProducts');
			  }else{
			    $_DL->renderTPL = $_DL->getCFGDef('tpl');
			  }
			  return $data;
  		}
	);

	return $modx->runSnippet('DocLister', $params);