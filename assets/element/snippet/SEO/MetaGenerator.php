<?php
/**
 * Формирование META-тегов для SEO
 */
header('X-Powered-By: http://agel-nash.ru');
$META = array();
//Кодировка
$META[] = '<meta charset="'.$modx->config['modx_charset'].'">';

//Канонические урлы для всех страниц с GET параметрами (кроме тех, где только $_GET['q'])
if(count($_GET)>1){
    $url = $modx->makeUrl($modx->documentObject['id']);
    $META[] = '<link rel="canonical" href="'.$url.'" />';
}

//Заголовок страницы
$title = isset($modx->documentObject['SEOTitle'][1]) ? $modx->documentObject['SEOTitle'][1] : '';
if(empty($title)){
    $title = $modx->getConfig('site_name');
    if($modx->documentObject['id'] != $modx->getConfig('site_start')){
        $title = $title.' | '.$modx->documentObject['pagetitle'];
    }
}
if(isset($_GET['page']) && $_GET['page']>0){
	$title .= ' | Страница '.(int)$_GET['page'];
	if($modx->getPlaceholder('totalPages')>0){
		$title .= ' из '.$modx->getPlaceholder('totalPages');
	}
}
$META[] = '<title>'.$title.'</title>';


//Описание документа
$desc = isset($modx->documentObject['SEODesc'][1]) ? $modx->documentObject['SEODesc'][1] : '';
if(empty($desc)){
    $desc = $modx->documentObject['introtext'];
}
$desc = e(strip_tags($desc));
if(!empty($desc)){
    $META[] = '<meta name="description" content="'.$desc.'" />';
}

//Ключевые слова
$key = isset($modx->documentObject['SEOKey'][1]) ? $modx->documentObject['SEOKey'][1] : '';
if(empty($key)){
    $key = $modx->documentObject['pagetitle'];
}
if(!empty($key)){
    $META[] = '<meta name="keywords" content="'.e($key).'" />';
}

//Индексировать ли страницу
$noIndex = isset($modx->documentObject['noIndex'][1]) ? $modx->documentObject['noIndex'][1] : '';
if(!empty($noIndex)){
    $META[] = $noIndex;
}

$META[] = '<meta name="generator" content="'.get_key($modx->getVersionData(), 'full_appname', 'MODX Evolution', 'is_scalar').'">';
$META[] = '<meta name="revisit-after" content="7 day">';
$META[] = '<meta name="author" content="Artem Stepochkin">';

$META[] = '<base href="'.$modx->getConfig('site_url').'" />';
return implode("", $META);