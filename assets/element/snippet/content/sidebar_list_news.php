<?php
/*
Шаблон списка новостей
*/
$currentID = $modx->documentObject['id'];
$params = array(
		'parents'=>$modx->documentObject['parent'],
		'display'=>3,
		'summary'=>'notags,len:150',
		'tvList'=>'news_date, item_image',
		'tvSortTYpe'=>'TVDATETIME',
		'orderBy'=>'news_date DESC',
		'addWhereList'=>"c.id != $currentID",
		'ownerTPL'=>'@CODE: <div class="rightcol-box">[+dl.wrap+]</div>',
		'tpl' => '@CODE: <a href="[+url+]" class="news-item">
									<div class="news-item__img"><img src="[+news_thumb+]" alt="[+e.title+]"></div>
									<div class="news-item__text">
										<p class="news-item__title">[+title+]</p>
									</div>
									<p class="news-item__date">[+ruDate+]</p>
								</a>
							',
		'tplNoImage' => '@CODE: <a href="[+url+]" class="news-item">
										<div class="news-item__text">
											<p class="news-item__title">[+title+]</p>
											<p class="news-item__teaser">[+summary+]</p>									
										</div>
										<p class="news-item__date">[+ruDate+]</p>
									</a>
								',					
		'prepare' => function($data,$modx,$_DL, $_eDL) {

						if (empty($data['tv.item_image'])) {
							$_DL->renderTPL = $_DL->getCFGDef('tplNoImage', $_DL->renderTPL);
						} else {
							$data['news_thumb'] = $modx->runSnippet('qThumb', array(
								'input'=>$data['tv.item_image'],
								'options'=>'w=230,h=163,zc=1'
							));
						}
						if ($data['tv.news_date']) {
							$data['ruDate'] = $modx->runSnippet('aDate',array(
								'date2'=>strtotime($data['tv.news_date']),
								'date'=>$data['pub_date'],
								'outFormat'=>'<b>%e% %m%</b> %y%'
							));
						}

						return $data;
		},				
	);


	$news = $modx->runSnippet('DocLister', $params);

	return $news;