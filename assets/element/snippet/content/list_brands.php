<?php
/*
List brands
*/
$params = array(
		'parents'=>$modx->documentObject['id'],
		'orderBy'=>'menuindex ASC',
		'ownerTPL'=>'@CODE: <div class="brands-row clearfix">[+dl.wrap+]</div>',
		'tpl' => '@CODE <div class="brands-item"><a title="[+e.title+]" href="[+url+]"><div class="brands-item__img"><img alt="[+e.title+]" src="[+thumb+]" title="[+e.title+]" /></div><p class="brands-item__more">Перейти к товарам</p></a></div>',		
		'tvList'=>'item_image',
		'prepare'=>function($data,$modx,$_DL, $_eDL) { 
			$data['thumb'] = $modx->runSnippet('qThumb', array(
				'input'=>$data['tv.item_image'],
				'options'=>'w=180'
			));
			return $data;
		}
	);

return $modx->runSnippet('DocLister', $params);