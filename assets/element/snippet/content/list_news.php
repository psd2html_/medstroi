<?php
/*
Шаблон списка новостей
*/

$params = array(
		'parents'=>$modx->documentObject['id'],
		'display'=>12,
		'summary'=>'notags,len:150',
		'tvList'=>'news_date, item_image',
		'tvSortTYpe'=>'TVDATETIME',
		'orderBy'=>'news_date DESC',
		'ownerTPL'=>'@CODE: <div class="news-list clearfix">[+dl.wrap+]</div>',
		'tpl' => '@CODE: 	<div class="news-col">
								<a href="[+url+]" class="news-item">
									<div class="news-item__img"><img src="[+news_thumb+]" alt="[+e.title+]"></div>
									<div class="news-item__text">
										<p class="news-item__title">[+title+]</p>
									</div>
									<p class="news-item__date">[+ruDate+]</p>
								</a>
							</div>',
		'tplNoImage' => '@CODE: <div class="news-col">
									<a href="[+url+]" class="news-item">
										<div class="news-item__text">
											<p class="news-item__title">[+title+]</p>
											<p class="news-item__teaser">[+summary+]</p>									
										</div>
										<p class="news-item__date">[+ruDate+]</p>
									</a>
								</div>',					
		'prepare' => function($data,$modx,$_DL, $_eDL) {

						if (empty($data['tv.item_image'])) {
							$_DL->renderTPL = $_DL->getCFGDef('tplNoImage', $_DL->renderTPL);
						} else {
							$data['news_thumb'] = $modx->runSnippet('qThumb', array(
								'input'=>$data['tv.item_image'],
								'options'=>'w=230,h=163,zc=1'
							));
						}
						if ($data['tv.news_date']) {
							$data['ruDate'] = $modx->runSnippet('aDate',array(
								'date'=>strtotime($data['tv.news_date']),
								'outFormat'=>'<b>%e% %m%</b> %y%'
							));
						}

						return $data;
		},
		'TplWrapPaginate'=>'@CODE: <ul class="pagination-list">[+wrap+]</ul>',
		'TplPage'=>'@CODE <li><a href="[+link+]" class="page">[+num+]</a></li>',
		'TplCurrentPage'=>'@CODE:  <li><span class="page">[+num+]</span></li>',
		'TplNextP'=>'@CODE <li><a href="[+link+]" class="page-arrow page-next">Вперед</a></li> ',
		'TplPrevP'=>'@CODE <li><a href="[+link+]" class=" page-arrow page-back">Назад</a></li>',
		'paginate'=>'pages'					
	);


	$news = $modx->runSnippet('DocLister', $params);
	$pagination = $modx->getPlaceholder('pages');

	return $news . $pagination;