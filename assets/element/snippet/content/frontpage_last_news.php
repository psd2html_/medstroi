<?php
$params = array(
		'parents'=>42,
		'display'=>3,
		'summary'=>'notags,len:150',
		'tvList'=>'news_date',
		'tvSortTYpe'=>'TVDATETIME',
		'orderBy'=>'news_date DESC',
		'ownerTPL'=>'@CODE: <div class="newslider-list">[+dl.wrap+]</div>',
		'tpl' => '@CODE: <div class="newslider-item">
			<a href="[+url+]">
				<span class="newslider-item__date">[+ruDate+]</span>
				<p class="newslider-item__title">[+title+]</p>
				<span class="newslider-item__teaser">[+summary+]</span>
			</a>
		</div>',
		'prepare' => function($data,$modx,$_DL, $_eDL) {
						if ($data['tv.news_date']) {
							$data['ruDate'] = $modx->runSnippet('aDate',array(
								'date2'=>strtotime($data['tv.news_date']),
								'date'=>$data['pub_date'],
								'outFormat'=>'%e% %m% %y%'
							));
						}

						return $data;
		},
	);


return $modx->runSnippet('DocLister', $params);
