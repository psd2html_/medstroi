<?php
$param_1 = array(
		'docid'=>$modx->documentObject['id'],
		'tvName'=>'product_gallery',
		'outerTpl'=>'@CODE <div class="product-thumbs"><div class="product-thumbs__slider">((wrapper))</div></div>',
		'rowTpl'=>'@CODE <div class="thumbs-link ((row.class))" data-image="img((row.number))"><img src="((thumbnail))" alt=""></div>',
		'firstClass'=>'select'
	);

$thumbnails = $modx->runSnippet('multiTV', $param_1);

$param_2 = array(
		'docid'=>$modx->documentObject['id'],
		'tvName'=>'product_gallery',
		'outerTpl'=>'@CODE <div class="product-bigimage">((wrapper))</div>',
		'rowTpl'=>'@CODE <img src="((full))" alt="" id="img((row.number))">'
	);
$bigImage = $modx->runSnippet('multiTV',$param_2);

return $thumbnails . $bigImage;