<?php
return $modx->runSnippet('evoSearch', array(
	'idType' => 'documents',
	'documents' => '',
	'ignoreEmpty' => 1,
	'addWhereList' => 'c.template = 14',
	'summary'=>'notags,len:10',
	'tvList'=>'item_image',
	'ownerTPL'=>'@CODE: <div class="b-content">
		<div class="b-search-res">
			<div class="prod-container">
				<div class="prod-row">[+dl.wrap+]</div>
			</div>
		</div>
	</div>',
	'tpl'=>'@CODE: <a href="[+url+]" class="prod-item" title="[+e.title+]">
		<div class="prod-item__img">
			<img src="[+tv.item_image+]" alt="[+e.title+]" title="[+e.title+]" alt="prod1.jpg">
		</div>
		<div class="prod-item__info">
			<p class="prod-item__title">[+title+]</p>
			<p class="prod-item__teaser">[+summary+]</p>
		</div>
		<span class="prod-item__more">узнать больше »</span>
	</a>',
	'show_stat' => 0,
	'addLikeSearch' => 1,
	'debug' => 0,
	'noneTPL' => '@CODE: <div class="b-search-res">
						<form action="/[~11~]" method="GET" class="search-form">
							<input type="text" name="search" id="searchword-res" class="input-txt">
							<button type="submit" class="btn btn-search">Найти</button>
						</form>
						<p class="red">Ничего не найдено</p>
					</div>',
	'noneWrapOuter' => 0,
));