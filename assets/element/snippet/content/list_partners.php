<?php
$data = $modx->documentObject;
$tplLink = '@CODE: <div class="partners-item"><a href="[+link+]" title="[+e.name+]"><div class="partners-img"><img src="[+image+]" alt="[+e.name+]"></div></a></div>';
$tplNoLink = '@CODE: <div class="partners-item"><div class="partners-img"><img src="[+image+]" alt="[+e.name+]"></div></div>';
				
$fieldsData = json_decode($data['partners'][1], true);
if (!empty($fieldsData['fieldValue'])) {
	foreach ($fieldsData['fieldValue'] as $field) {
		$field['image'] = $modx->runSnippet('qThumb', array(
				'input' => $field['image'],
				'options'=>'w=220'
			));
		$out .= $modx->tpl->parseChunk(empty($field['link']) ? $tplNoLink : $tplLink, $field);
	}
}
return $out;