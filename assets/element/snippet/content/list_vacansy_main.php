<?php

return $modx->runSnippet('DocLister', array(
	'idType' => 'parents',
	'parents' => 43,
	'orderBy' => 'c.menuindex ASC',
	'tvList' => 'duty,condition',
	'debug' => 0,
	'noneWrapOuter' => 0,
	'noneTPL' => '@CODE: ',
	'summary'=>'notags,len:100',
	'ownerTPL' => '@CODE: <div class="work-wrap">
		<h3>Вакансии</h3>
		<div class="workslider-list">[+dl.wrap+]</div>
	</div>',
	'tpl' => '@CODE: <div class="workslider-item">
		<a href="/[~43~]#lists" title="Подробнее">
			<p class="workslider-item__title">[+title+]</p>
			<p class="workslider-item__teaser">[+summary+]</p>
			<span class="workslider-item__more">подробнее</span>
		</a>
	</div>'
));