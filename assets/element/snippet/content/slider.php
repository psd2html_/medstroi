<?php
$slider = isset($modx->documentObject['front_slider'][1]) ? $modx->documentObject['front_slider'][1] : '';
$slider = jsonHelper::jsonDecode($slider, array('assoc' => true));
$out = '';
$linkTPL = '@CODE: <a href="[+link+]" class="topslider-item">
								<img src="[+image+]" alt="[+e.title+]">
								<span class="topslider-item__text">
									<p class="topslider-item__title">[+title+]</p>
									<span class="topslider-item__more">Узнать больше</span>
								</span>
							</a>';
$noLinkTPL = '@CODE: <span class="topslider-item">
								<img src="[+image+]" alt="[+e.title+]">
								<span class="topslider-item__text">
									<p class="topslider-item__title">[+title+]</p>
								</span>
							</span>';
if(!empty($slider) && !empty($slider['fieldValue'])){
    foreach($slider['fieldValue'] as $num => $step){
        $out .= $modx->tpl->parseChunk(get_key($step, 'link', '')!= '' ? $linkTPL : $noLinkTPL,
                array(
                    'title' => get_key($step, 'title', ''),
                    'image' => get_key($step, 'image', ''),
                    'e.title' => htmlentities(get_key($step, 'title', ''), ENT_COMPAT, 'UTF-8', false),
                    'link' => get_key($step, 'link', ''),
                    'desc' => get_key($step, 'desc', ''),
                ), true
            );
    }
    if(!empty($out)){
        $out = $modx->tpl->parseChunk('@CODE: <div class="topslider-wrap">[+wrap+]</div><div class="topslider-pager"></div>', array('wrap' => $out, 'doc' => $modx->documentObject), true);
    }
}
return $out;