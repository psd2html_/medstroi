<?php
	
$i = 0;
$out = with(new \DLCollection($modx, get_key(
	\jsonHelper::jsonDecode(
		get_key(get_key($modx->documentObject, 'contacts', array(), 'is_array'), 1, '', 'is_scalar'),
		array( 'assoc' => true )
	), 'fieldValue', array(), 'is_array'
)))->reduce(function($out, $val) use ($modx, &$i){
	$i++;
	if($i == 1){
		$val['row.class'] = 'lass="select"';
	}
	$out .= $modx->tpl->parseChunk('@CODE:<li [+row.class+] onClick="changeMap( [+coords+] );">
		<h4>[+office+]:</h4>
		<p>[+address+]<br>
		Тел.: <a href="tel:[+phone+]" class="tel-link">[+phone+]</a></p>
		</li>', $val);
    return $out;
});
if(!empty($out)){
	$out = $modx->tpl->parseChunk('@CODE: <ul class="adress-list">[+out+]</ul>', compact('out'));
};
return $out;
/*
$args = array(
		'docid'=>$modx->documentObject['id'],
		'tvName'=>'contacts',
		'outerTpl'=>'@CODE <ul class="adress-list">((wrapper))</ul>',
		'rowTpl'=>'@CODE: 	<li class="((row.class))" onClick="changeMap( ((coords)) );">
								<h4>((office)):</h4>
								<p>((address))<br>
								Тел.: <a href="tel:((phone))" class="tel-link">((phone))</a></p>
							</li>',
		'firstClass'=>'select'
	);


return $modx->runSnippet('multiTV', $args);
*/