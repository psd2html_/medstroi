<?php

return $modx->runSnippet('DocLister', array(
	'idType' => 'parents',
	'parents' => 43,
	'orderBy' => 'c.menuindex ASC',
	'tvList' => 'duty,condition',
	'debug' => 0,
	'noneWrapOuter' => 0,
	'noneTPL' => '@CODE: ',
	'ownerTPL' => '@CODE: <a name="lists"></a><div class="b-vacancy">
		<div class="wrap">
			<h2>На данный момент в компании открыты следующие вакансии:</h2>
			<div class="vacancy-acc">[+dl.wrap+]</div>
		</div>
	</div>',
	'tpl' => '@CODE: <h3 class="acc-title [+p.class+]">[+title+]</h3>
		<div class="acc-content" [+p.style+]>
			<div class="row clearfix">
				<div class="vacancy-col">
					<h4>Требования к кандидатам:</h4>
					[+content+]
				</div>
				<div class="vacancy-col">
					<h4>Обязанности:</h4>
					[+tv.duty+]
				</div>
			</div>
			<div class="vacancy-col--wide">
				<h4>Условия:</h4>
				<ul>
					[+tv.condition+]
				</ul>
			</div>
		</div>',
		'prepare' => function($data, $modx, $_DL){
			if($data['iteration'] == 1){
				$data['p.class'] = 'open-acc';
				$data['p.style'] = 'style="display:block;"';
			}
			$condition = json_decode($data['tv.condition'], true);
			$condition = get_key($condition, 'fieldValue', array(), 'is_array');
			$data['tv.condition'] = '';
			foreach($condition as $item){
				$data['tv.condition'].= $_DL->parseChunk('@CODE: <li>[+if+]</li>', $item);
			}
			return $data;
		}
));