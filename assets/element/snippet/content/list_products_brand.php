<?php
/*
Шаблон вывода товаров в категориях
*/
	$brandID = $modx->documentObject['id'];
	$params = array(
			'idType'=>'parents',
			'parents'=>'15, 16',	
			'display'=>9,
			'summary'=>'notags,len:10',
			'tvList'=>'item_image,select_brends',
			'filters'=>"AND(tv:select_brends:=:$brandID)",
			'ownerTPL'=>'@CODE <div class="prod-container"><div class="prod-row">[+dl.wrap+]</div></div>',
			'tpl'=>'@CODE 	<a href="[+url+]" class="prod-item">
								<div class="prod-item__img"><img src="[+product_thumb+]" alt="[+e.title+]"></div>
								<div class="prod-item__info">
									<p class="prod-item__title">[+e.title+]</p>
									<p class="prod-item__teaser">[+summary+]</p>
								</div>
								<span class="prod-item__more">узнать больше »</span>
							</a>',			
			'prepare'=>function($data,$modx,$_DL, $_eDL) {
							$data['product_thumb'] = $modx->runSnippet('qThumb', array(
								'input'=>$data['tv.item_image'],
								'options'=>'w=180,h=132,zc=1'
							));
						return $data;					
			},			
			'TplWrapPaginate'=>'@CODE: <ul class="pagination-list">[+wrap+]</ul>',
			'TplPage'=>'@CODE <li><a href="[+link+]" class="page">[+num+]</a></li>',
			'TplCurrentPage'=>'@CODE:  <li><span class="page">[+num+]</span></li>',
			'TplNextP'=>'@CODE <li><a href="[+link+]" class="page-arrow page-next">Вперед</a></li> ',
			'TplPrevP'=>'@CODE <li><a href="[+link+]" class=" page-arrow page-back">Назад</a></li>',
			'paginate'=>'pages'						
		);

	$products = $modx->runSnippet('DocLister', $params);
	$pagination = $modx->getPlaceholder('pages');

	return $products . $pagination;