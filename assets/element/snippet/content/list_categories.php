<?php
/*
Выводим список всех категорий с картинками документа "Категории"
*/
$params = array(
		'parents'=>$modx->documentObject['id'],
		'orderBy'=>'menuindex ASC',
		'ownerTPL'=>'@CODE: <div class="catalog-row clearfix">[+dl.wrap+]</div>',
		'tpl' => '@CODE <div class="catalog-item"><a title="[+e.title+]" href="[+url+]"><div class="catalog-item__img"><img alt="[+e.title+]" src="[+thumb+]" title="[+e.title+]" /></div><p class="catalog-item__title">[+title+]</p></a></div>',		
		'tvList'=>'item_image',
		'prepare'=>function($data,$modx,$_DL, $_eDL) { 
			$data['thumb'] = $modx->runSnippet('qThumb', array(
				'input'=>$data['tv.item_image'],
				'options'=>'w=234&h=118&zc=1'
			));
			return $data;
		}
	);

return $modx->runSnippet('DocLister', $params);