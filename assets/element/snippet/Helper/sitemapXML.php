<?php
return $modx->runSnippet('DocLister', array(
	'controller' => 'onetable',
	'parents' => '',
	'idType' => 'parents',
	'ignoreEmpty' => 1,
	'debug' =>0,
	'addWhereList' => 'id NOT IN(2,3,5,6,92,93,94,95,96) AND template NOT IN(12,5,6,4,13,11,9,8,0)',
	'tpl' => '@CODE:<url><loc>[+url+]</loc></url>',
	'ownerTPL' => '@CODE:<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">[+dl.wrap+]</urlset>',
	'prepare' => '\OMLanding\DLPrepare::sitemapXML',
));

