<?php

return $modx->runSnippet('DLBuildMenu', array(
	'parents' => 0,
	'debug' => 0,
	'addWhereList' => 'c.id NOT IN(2,3,5,6,92,93,94,95,96) AND c.template NOT IN(12,6,4,13,11,9,8,0)',
	'AfterPrepare' => '\OMLanding\DLPrepare::sitemap',
	'noLinkTPL' => '@CODE: <li id="menu-item-[+id+]" class="menu-item [+dl.class+]">
        <span>[+title+]</span>
        [+dl.submenu+]
    </li>'
));