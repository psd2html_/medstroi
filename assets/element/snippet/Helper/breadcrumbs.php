<?php
	$params = array(
			'tpl'=>'@CODE: <a href="[+url+]" title="[+e.title+]">[+title+]</a> »',
			'showCurrent'=>1,
			'tplCurrent'=>'@CODE: <span>[+title+]</span>',
			'ownerTPL'=>'@CODE: <div class="b-breadcrumbs">[+crumbs.wrap+]</div>',
		);

	return $modx->runSnippet('DLcrumbs',$params);