<?php
/**
Проверяем, есть ли картинка у записи
[[if_image? &imageField=`[tvImageField]` &options=`qThumb options` &alt=`` &class=``]]
*/

$class = (!empty($class)) ? ' class="'.$class.'"': '';
$alt = (!empty($alt)) ? ' alt="'.$alt.'"':'""';
$image = "";
if ($imageField) {
			$thumb = $modx->runSnippet('qThumb', array(
				'input' => $imageField,
				'options'=> $options
			));
	$image = '<img src="'.$thumb.'"'.$class.$alt.' />';		
}
return $image;