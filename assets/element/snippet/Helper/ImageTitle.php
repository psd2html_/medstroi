<?php
$out = '';
$image = get_key($modx->documentObject, 'item_image', array(), 0);
$image = get_key($image, 1, '', 'is_scalar');
if(!empty($image)){
	$out = $modx->tpl->parseChunk('@CODE: <div class="cat-info">
			<img src="[+image+]" alt="[+e.pagetitle+]" class="cat-img">
			<h1 class="cat-title">[+pagetitle+]</h1>
		</div>', array(
		'image' => $modx->runSnippet('qThumb', array('input'=>$image, 'options'=>'w=960h=296')),
		'pagetitle' => $modx->documentObject['pagetitle'],
		'e.pagetitle' => e($modx->documentObject['pagetitle'])
	));
}
return $out;