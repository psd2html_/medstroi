<?php
$data = array(
	'alt' => $modx->getConfig('site_name'),
	'e.alt' => e($modx->getConfig('site_name'))
);
$title= $modx->doc->edit($modx->getConfig('site_start'))->get('SEOTitle');
$data['title'] = empty($title) ? $data['alt'] : $title;
$data['e.title'] = e($data['title']);
$data['url'] = $modx->getConfig('site_url');

$tpl = ($modx->documentObject['id'] != $modx->getConfig('site_start')) ? 'pageTpl' : 'mainTpl';
$tpl = isset($modx->event->params[$tpl]) ? $modx->event->params[$tpl] : '';

return $modx->tpl->parseChunk($tpl, $data);