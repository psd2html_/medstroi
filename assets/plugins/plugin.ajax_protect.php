<?php
include_once(MODX_BASE_PATH."assets/lib/APIHelpers.class.php");
switch($modx->event->name){
	case 'OnWebPagePrerender':{
		if(empty($parent)) return false;
		$content = $modx->documentOutput;
		$ajaxPages = array();
		if(!empty($modx->cache) && $modx->cache->contains('ajax_pages')){
			$ajaxPages = $modx->cache->fetch('ajax_pages');
		}
		if(empty($ajaxPages)){
			$out = $modx->runSnippet('DocLister', array(
				'idType' => 'parents',
				'parents' => $parent,
				'depth' => 10,
				'showParent' => 1,
				'api' => 'id'
			));
			$ajaxPages = array_keys(json_decode($out, true));
			$modx->cache->save('ajax_pages', $ajaxPages);
		}
		foreach($ajaxPages as $idPage){
			if(!empty($modx->cache) && $modx->cache->contains('url_page_'.$idPage)){
				$url = $modx->cache->fetch('url_page_'.$idPage);
			}
			if(empty($url)){
				$url = $modx->makeUrl($idPage);
				$modx->cache->save('url_page_'.$idPage, $url);
			}
			$content = preg_replace_callback('/(<a\s[^>]*href=(\"??)([^\" >]*?)[^>]*>)(.*)(<\/a>)/iUs', function($match) use($url, $modx){
				if($match[3] == $url || $match[3] == $modx->getConfig('site_url').ltrim($url, '/')){
					$match[1] = substr($match[1], 0, -1);
					$match[1] .= ' data-modx="ajax"';
					$match[1] .= '>';
				}
				return $match[1].$match[4].$match[5];
			}, $content);
			$url = null;
		}
		$content = preg_replace_callback('/(<body(.*)>)/iUs', function($match) use($modx){
			$match[0] = substr($match[1], 0, -1);
			$match[0] .= ' data-no-instant data-modx-docid="'.$modx->documentIdentifier.'"';
			$match[0] .= '>';
			return $match[0];
		}, $content);

		$content = preg_replace_callback('/(<body(.*)>)/iUs', function($match) use($modx){
			$match[0] = substr($match[1], 0, -1);
			$match[0] .= ' data-modx-referer="'.e(get_key($_SERVER, 'HTTP_REFERER', '', 'is_scalar')).'"';
			$match[0] .= '>';
			return $match[0];
		}, $content);
		$modx->documentOutput = $content;

		if(!empty($modx->jsonMode)){
			header('Content-type: application/json');
			if( ! is_array($modx->jsonMixed)){
				$modx->jsonMixed = array();
			}
			$modx->documentOutput = json_encode(array_merge($modx->jsonMixed, array(
				'status' => $modx->jsonStatus,
				'text' => $modx->documentOutput
			)));
		}
		break;
	}
	case 'OnWebPageInit':
	case 'OnPageNotFound':{
		if((int)$modx->documentIdentifier <= 0 || empty($parent)) return false;

		if(!empty($modx->cache) && $modx->cache->contains('topParent'.$modx->documentIdentifier)){
			$parentID = $modx->cache->fetch('topParent'.$modx->documentIdentifier);
		}
		if(empty($parentID)){
			$parentID = $modx->runSnippet('takeParent', array(
				'id' => $modx->documentIdentifier,
				'topLevel' => 1
			));
			$modx->cache->save('topParent'.$modx->documentIdentifier, $parentID);
		}
		if($parentID == $parent){
			if(strtolower(\APIhelpers::getkey($_SERVER, 'HTTP_X_REQUESTED_WITH', null)) === 'xmlhttprequest'){
				$modx->jsonMode = false;
			}else{
				$object = $modx->getDocumentObject('id', $modx->documentIdentifier);
				if(!empty($object['link_attributes'])){
					$link = $modx->tpl->parseDocumentSource($object['link_attributes']);
				}else{
					 $link = '';
				}
				if(empty($link)){
					$modx->documentIdentifier = 0;
					$modx->sendErrorPage();
				}else{
					$modx->sendRedirect($link, 0, '', "HTTP/1.1 307 Temporary Redirect");
				}
			}
		}
		break;
	}
}