;(function($){
    $(function() {
        function ajaxLink(){
            $('body').on('click', 'a:data(modx=ajax)', function(e){
                e.preventDefault();
                $.ajax({
                    url: $(this).attr('href'),
                    data: {
                        data: $(this).data(),
                        main: $("body").data()
                    },
                    type: "GET"
                }).done(function(responce){
                    $.sender.fancy(responce);
                   initAjax();
                }).fail(function(){
                    $.ANMsg.error($.sender.lang.errorLoad);
                });
            });
        }
        ajaxLink();

        /** AJAX отправка формы */
        function ajaxFormSend(){
            var validateForm = $('form:not(.no-validate)').not(':data(modx=ajax)');
            if(validateForm.length){
                var noAjaxConfig = $.extend({}, $.validatorConfig, {
                    submitHandler: function(form){
                        form.submit();
                    }
                });
                validateForm.each(function(index) {
                    $(this).validate(noAjaxConfig);
                });
            }

            var ajaxForm = $('form:data(modx=ajax)');
            if(ajaxForm.length){
                ajaxForm.each(function(index) {
                    $(this).validate($.validatorConfig);
                });
            }
        }

        $('body').on('click', '.modal-link', function(e){
            e.preventDefault();
            var selector = $.jHelper.defaultData($(this), 'inline', '');
            if(selector !== '' && $(selector).length){
                $.sender.fancy($(selector).html());
                ajaxFormSend();
            }
        });

        function initAjax(){
            $("input[name=phone]").mask("+7 (999) 999-99-99");
            ajaxFormSend();
        }
        initAjax();

        $('a.fancy').fancybox();

        $('body').on('click', '.noclick', function(e){
            e.preventDefault();
        });


        $('body').on('click', 'a:data(modx=tab)', function(e){
            e.preventDefault();
			var _this = this;
            var url = $(_this).attr('href');
            $.ajax({
                url: url,
                type: "GET"
            }).done(function(responce){
                if ('history' in window && 'pushState' in window.history) {
                    window.history.pushState({}, '', url);
                } else {
                    window.location = url;
                }
                $('title').text($(responce).filter('title').text());
				$('li', $(_this).parents('.sub-menu')).removeClass('active');
                $(_this).parent().addClass('active');
                $('#ajax-tab').fadeToggle('slow', function(){
                    $('#crumbs').html($('#crumbs', responce).html());
                    $('#ajax-tab').html($('#ajax-tab', responce).html());
                    if($('.vacancy-col--wide ul').length){
                        $('.vacancy-col--wide ul').autocolumnlist({ columns: 2 });
                    }
                    $(window).trigger('resize');
                }).fadeToggle('slow');
                initAjax();
            }).fail(function(){
                $.ANMsg.error($.sender.lang.errorLoad);
            });
        });
    });
})(jQuery);