<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'if' => array(
        'caption' => 'Условие',
        'type' => 'text'
    )
);

$settings['configuration'] = array(
    'enablePaste' => false,
    'enableClear' => false
);