<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(

    'name' => array(
        'caption' => 'Партнер',
        'type' => 'text',
    ),
    'image' => array(
        'caption' => 'Логотип',
        'type' => 'image', 
    ),

    'link' => array(
        'caption'=> 'Ссылка',
        'type'=> 'text'
    ),
);
$settings['templates'] = array(
    'outerTpl' => '<div class="partners-list__inside clearfix">[+wrapper+]</div>',
    'rowTpl' => '<div class="partners-item">
                    <a href="[+link+]"><div class="partners-img"><img src="[+image+]" alt="[+e.name+]"> [+thumb+]</div></a>
                </div>'
);
