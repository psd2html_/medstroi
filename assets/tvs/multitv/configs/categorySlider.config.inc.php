<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'image' => array(
        'caption' => 'Слайд',
        'type' => 'image'
    ),
    'title' => array(
        'caption' => 'Заголовок',
        'type' => 'text'
    ),
    'docs' => array(
        'caption' => 'ID страниц услуг для слайдера',
        'type' => 'text'
    )
);

$settings['configuration'] = array(
    'enablePaste' => false,
    'enableClear' => false
);