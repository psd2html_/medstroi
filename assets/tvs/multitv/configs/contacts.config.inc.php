<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'office' => array(
        'caption' => 'Название филлиала',
        'type' => 'text'
    ),
    'address' => array(
        'caption' => 'Адрес филлиала',
        'type' => 'text'
    ),
    'phone' => array(
        'caption' => 'Номер телефона',
        'type' => 'text'
    ),        
    'coords' => array(
        'caption' => 'Координаты яндекс.карты',
        'type' => 'text'
    ));
