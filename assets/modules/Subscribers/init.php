<?php
if (IN_MANAGER_MODE != "true" || empty($modx) || !($modx instanceof DocumentParser)) {
    die("<b>INCLUDE_ORDERING_ERROR</b><br /><br />Please use the MODX Content Manager instead of accessing this file directly.");
}
if (!$modx->hasPermission('exec_module')) {
    header("location: " . $modx->getManagerPath() . "?a=106");
}
if(!is_array($modx->event->params)){
    $modx->event->params = array();
}
$ajax = (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
\Subscribers\Helper::init($modx);
$TPL = new \Subscribers\Template($modx, $ajax, dirname(__FILE__));
\Subscribers\Action::init($modx, $TPL, new \MedStroy\MODxAPI\modSubscribe($modx));
if (!empty($action) && method_exists('\Subscribers\Action', $action)) {
    $data = call_user_func_array(array('\Subscribers\Action', $action), array());
    if (!is_array($data)) {
        $data = array();
    }
} else {
    $data = array();
}
$tpl = \Subscribers\Action::$TPL;
if(!is_null($tpl)){
    $out = $TPL->showHeader();
    $out .= $TPL->showBody($tpl, $data);
    $out .= $TPL->showFooter();
}else{
    header('Content-type: application/json');
    $out = json_encode($data);
}
echo $out;