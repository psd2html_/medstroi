<?php namespace Subscribers;
class Action extends \Module\Action
{
	protected static $TABLE = "subscribers";
    public static function fullDelete()
    {
        $data = array();
        $dataID = (int)Template::getParam('docId', $_GET);
        if ($dataID > 0 && self::_checkObj($dataID)) {
            $modRedirect = self::$classTable;
            $modRedirect->delete($dataID);
            if (!self::_checkObj($dataID)) {
                $data['log'] = 'Удалена запись с ID: <strong>' . $dataID . '</strong>';
            } else {
                $data['log'] = 'Не удалось удалить запись с ID: <strong>' . $dataID . '</strong>';
            }
        } else {
            $data['log'] = 'Не удалось определить обновляему запись';
        }
        return $data;
    }
}