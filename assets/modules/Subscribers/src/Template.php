<?php namespace Subscribers;
class Template extends \Module\Template
{
    public function Lists()
    {
        $out = '';
        $this->_modx->documentIdentifier = $this->_modx->getConfig('site_start');
        $this->_modx->config['site_url'] = MODX_MANAGER_URL;
        $method = $this->getParam('method', $_GET, '');
        /**
         * Хакаем URL пагинатора
         */
        parse_str(parse_url(MODX_SITE_URL . $_SERVER['REQUEST_URI'], PHP_URL_QUERY), $URL);
        $_SERVER['REQUEST_URI'] = $this->_modx->getManagerPath() . "?" . http_build_query(array_merge($URL, array('q' => null, 'action' => null)));
        $out = $this->_modx->runSnippet('DocLister', array(
            'controller' => 'onetable',
            'table' => Action::TABLE(),
            'orderBy' => '`email` ASC',
            'tpl' => '@CODE: ' . $this->showBody('table/body'),
            'ownerTPL' => '@CODE: ' . $this->showBody('table/wrap'),
            'altItemClass' => 'gridAltItem',
            'itemClass' => 'gridItem',
            'display' => self::getParam('display', $this->_modx->event->params),
            'id' => 'dl',
            'pageInfoTpl' => '@CODE: ' . $this->showBody('table/pageInfo'),
            'pageInfoEmptyTpl' => '@CODE: ' . $this->showBody('table/pageInfoEmpty'),
            'debug' => 0,
            'noneTPL' => '@CODE: Нет данных',
            'noneWrapOuter' => 0,
            'paginate' => 'pages',
            'prepare' => function (array $data = array(), \DocumentParser $modx, \onetableDocLister $_DocLister) {
                $data['class'] = (isset($data['dl.iteration']) && $data['dl.iteration'] % 2) ? $_DocLister->getCFGDef('itemClass') : $_DocLister->getCFGDef('altItemClass');
                return $data;
            },
            'idType' => 'documents',
            'ignoreEmpty' => 1
        ));
        $out .= $this->_modx->getPlaceholder('dl.pages');
        return $out;
    }
}