/* Thanks to CSS Tricks for pointing out this bit of jQuery
It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */

equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}
$.fn.equivalent = function (){
    var $blocks = $(this),
        maxH    = $blocks.eq(0).height(); 

    $blocks.each(function(){
        maxH = ( $(this).height() > maxH ) ? $(this).height() : maxH;
    });
    return maxH;
}

/* animate auto */
function autoHeightAnimate(element, time){
  	var curHeight = element.height(), // Get Default Height
        autoHeight = element.css('height', 'auto').height(); // Get Auto Height
    	  element.height(curHeight); // Reset to Default Height
    	  element.stop().animate({ height: autoHeight }, parseInt(time)); // Animate to Auto Height
}

/* functions */
function popupClose(e){
	$(this).closest(".wrap-popup").fadeOut(400, function(){$('html').removeClass('popup-open');});
	e.preventDefault();
}

$(window).load(function(){
	equalheight('.prod-item');
	equalheight('.brands-item');
});

$(document).ready(function () { 
	width = Math.max($(window).width(), window.innerWidth);

	/***********
	** common
	***********/

	//placeholder ie
  	$('[placeholder]').placeholder();

	//open mob-menu
	$('.mainmenu-mob-toggle').on('click', function(e){
		$(this).toggleClass('open-m-m');
		$('.mainmenu').toggle();
		if($('.search-mob-toggle').hasClass('open-s-m')){
			$('.header-right__search').hide();
			$('.search-mob-toggle').removeClass('open-s-m');
		}
	});
	$('.close-menu').on('click', function(e){
		$('.mainmenu').hide();
	});

	//open mob-search
	$('.search-mob-toggle').on('click', function(e){
		$(this).toggleClass('open-s-m');
		$('.header-right__search').toggle();
		if($('.mainmenu-mob-toggle').hasClass('open-m-m')){
			$('.mainmenu').hide();
			$('.mainmenu-mob-toggle').removeClass('open-m-m');
		}
	});

	//popup
	$('.wrap-popup .close').on('click', popupClose);
	$(".wrap-popup-bg").on('click', popupClose);

	//scroll to item
	$("a.scroll-link").on('click', function(e) {
	var anchor = $(this);
	var topOffset = 30;
	$('html, body').stop().animate({
	  scrollTop: ($(anchor.attr('href')).offset().top - topOffset)
			}, 600, 'easeOutQuad');
	  e.preventDefault();
	});

	//contact tabs
	$('.adress-list li').on('click', function() {
		$('.adress-list li').not(this).removeClass('select');
		$(this).addClass('select');
	});

	//topslider
	var topslider = $('.topslider-wrap').bxSlider({
		auto: true,
		controls: true,
		pagerSelector: '.topslider-pager',
		nextSelector: '.topslider-next',
		prevSelector: '.topslider-prev',
		mode: 'vertical',
		autoDirection: 'prev',
		speed: 1000
	});

	//newslider
	var newslider = $('.newslider-list').bxSlider({
		auto: false,
		pager: false,
		controls: true,
		nextSelector: '.newslider-next',
		prevSelector: '.newslider-prev'
	});

	//topslider
	var workslider = $('.workslider-list').bxSlider({
		auto: false,
		controls: true,
		pagerType: 'short',
		pagerSelector: '.workslider-pager',
		nextSelector: '.workslider-next',
		prevSelector: '.workslider-prev'
	});

	//tabs
	$(".tab-link").on('click', function(e){
		e.preventDefault();
		$('.tab-link').parent('li').removeClass('active');
		$(this).parent('li').addClass('active');
		var tab = $(this).attr('href');
		$(".tab:not("+tab+")").fadeOut(500); 
		$(tab).delay(500).fadeIn(700);		
	});

	//acc vacancy
	$('.acc-title').on('click', function(){
		var accLink = $(this);
		$('.acc-title').not(this).removeClass('open-acc');	
		accLink.toggleClass('open-acc'); 
		$('.acc-title').not(this).next('.acc-content').slideUp();	
		accLink.next('.acc-content').slideToggle();
	});

	//acc vacancy list
	$('.vacancy-col--wide ul').autocolumnlist({ columns: 2 });

	//thumb
	$('.thumbs-link').on('click', function(){
		var img = $(this).data('image');
		$('.product-bigimage img').hide();
		$('.thumbs-link').removeClass('select');
		$(this).addClass('select');
		$('#'+img).css('display', 'inline-block');
	});
	var thumbslider = $('.product-thumbs__slider').bxSlider({
		minSlides: 4,
		maxSlides: 4,
		slideWidth: 100,
		mode: 'vertical',
		pager: false,
		controls: true,
		slideMargin: 8,
		speed: 1000
	});

	//ajax example for contact form
	/* $('#contact-form').submit(function(e) {
		e.preventDefault();
		var fdata = $(this).serialize();
		$.ajax({
			type: "POST",
     		url: "",
			data: fdata,
			error: function(msg) {
				//error handler
			},
			success: function(msg) {
					$('#success-popup').fadeIn();
					$('#contact-form').trigger( 'reset' );
				}
			});
	});*/

	//check load and resize - for all pages
	function checkWindowSize() {
		width = Math.max($(window).width(), window.innerWidth);
		height = Math.max($(window).height(), window.innerHeight);

		$('.mainmenu-dd').width(width);

		/* dd menu */
		if(width > 1000){
			$('.mainmenu .has-child > a').off();
			$('.mainmenu .has-child').off().on({
			    mouseenter: function () {
			      	$(this).find('.mainmenu-dd').fadeIn(350);
			    },
			    mouseleave: function () {
			      	$(this).find('.mainmenu-dd').fadeOut(350);
			    }
			});
		}else{
			$('.mainmenu .has-child').off();
			$('.mainmenu .has-child > a').off().on('click', function(e){
				e.preventDefault();
				$(this).next('.mainmenu-dd').fadeToggle();
			});
		}

		/* visible check */
		if(width > 1000){
			$('.mainmenu').show();
			$('.mainmenu-dd').hide();
		}
		if(width > 750){
			$('.header-right__search').show();
		}else{
			$('.header-right__search').hide();
		}

		/* menu columns */
		if(width > 750){
			$('.mainmenu-dd__childmenu.cat ul').autocolumnlist({ columns: 2 });
		}

		/* partners width */
		var wrapWidth = $('.wrap').width();
		if(width > 1000){
			$('.partners-info__inside').width(wrapWidth*0.5);
			$('.partners-list__inside').width(wrapWidth*0.5);
			$('.company-box__inside').width(wrapWidth*0.5);
			$('.product-box__inside').width(wrapWidth*0.5);
		}else if(width > 750 && width <= 1000){
			$('.partners-info__inside').width((wrapWidth-20)*0.5);
			$('.partners-list__inside').width((wrapWidth-20)*0.5);
			$('.company-box__inside').width((wrapWidth-20)*0.5);
			$('.product-box__inside').width((wrapWidth-20)*0.5);
		}else{
			$('.partners-info__inside').width('auto');
			$('.partners-list__inside').width('auto');
			$('.company-box__inside').width('auto');
			$('.product-box__inside').width('auto');
		}


		/* pager position */
		var pagerHeight = $('.topslider-pager').height();
		$('.topslider-pager').css('margin-top', '-' + pagerHeight/2 + 'px');
	}
	checkWindowSize();

	//resize
	$(window).resize(function(e) {
		if(width > 750){
			$('.mainmenu-dd__childmenu.cat li').unwrap();
		}
		equalheight('.prod-item');
		equalheight('.brands-item');
		if ($('.topslider-wrap').length > 0) {
		    topslider.reloadSlider();
		}
		if ($('.newslider-list').length > 0) {
		    newslider.reloadSlider();
		}
		if ($('.workslider-list').length > 0) {
		    workslider.reloadSlider();
		}
		
		checkWindowSize();
	});
});